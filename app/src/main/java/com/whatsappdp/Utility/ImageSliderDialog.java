package com.whatsappdp.Utility;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.fangxu.allangleexpandablebutton.AllAngleExpandableButton;
import com.fangxu.allangleexpandablebutton.ButtonData;
import com.fangxu.allangleexpandablebutton.ButtonEventListener;
import com.whatsappdp.Activity.HomeActivity;
import com.whatsappdp.Modal.Upload;
import com.whatsappdp.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public abstract class ImageSliderDialog extends Dialog {

    private HomeActivity activity = null;
    private ArrayList<Upload> jobImagesModels = null;
    private ViewPager vpImageSlider = null;
    private ImageView imgSliderClose = null;
    private int position = 0;


    protected ImageSliderDialog(Context context) {
        super(context);
        activity = (HomeActivity) context;
    }

    protected ImageSliderDialog(Context context, ArrayList<Upload> postModel, int positions) {
        super(context);
        this.activity = (HomeActivity) context;
        this.jobImagesModels = postModel;
        this.position = positions;
    }

    public ImageSliderDialog(Context context, int theme) {
        super(context, theme);
        activity = (HomeActivity) context;
    }

    protected ImageSliderDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        activity = (HomeActivity) context;
    }

    public Activity getActivity() {
        return activity;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        View view = activity.getLayoutInflater().inflate(R.layout.dialog_image_slider, null);

        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        getWindow().setGravity(Gravity.FILL_HORIZONTAL);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        vpImageSlider = view.findViewById(R.id.vpImageSlider);
        imgSliderClose = view.findViewById(R.id.imgSliderClose);
        setContentView(view);
        vpImageSlider.setAdapter(new PostDetailsPagerAdapter(activity, jobImagesModels, position));
        vpImageSlider.setCurrentItem(position, true);
        imgSliderClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        AllAngleExpandableButton button = findViewById(R.id.button_expandable);
        setFlabButtons(button);
    }

    private void setFlabButtons(AllAngleExpandableButton button) {
        try {
            final List<ButtonData> buttonDatas = new ArrayList<>();
            int[] drawable = {R.drawable.plus, R.drawable.ic_share, R.drawable.ic_fav};
            int[] color = {R.color.blue, R.color.green, R.color.red};
            for (int i = 0; i < 3; i++) {
                ButtonData buttonData;
                if (i == 0) {
                    buttonData = ButtonData.buildIconButton(activity, drawable[i], 15);
                } else {
                    buttonData = ButtonData.buildIconButton(activity, drawable[i], 0);
                }
                buttonData.setBackgroundColorId(activity, color[i]);
                buttonDatas.add(buttonData);
            }
            button.setButtonDatas(buttonDatas);
            setListener(button);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        activity = null;
    }

    private void setListener(AllAngleExpandableButton button) {
        button.setButtonEventListener(new ButtonEventListener() {
            @Override
            public void onButtonClicked(int index) {

                if (index == 2) {


                    if (isStoragePermissionGranted()) {
                        Glide.with(activity).load(jobImagesModels.get(vpImageSlider.getCurrentItem()).getUrl()).into(new SimpleTarget<Drawable>() {
                            @Override
                            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                                shareImageWhatsApp(resource);
                            }

                            @Override
                            public void onLoadFailed(@Nullable Drawable errorDrawable) {
                                super.onLoadFailed(errorDrawable);

                            }

                            @Override
                            public void onLoadStarted(@Nullable Drawable placeholder) {
                                super.onLoadStarted(placeholder);
                            }
                        });
                    } else {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    }


                }
                if(index==1)
                {
                    if (isStoragePermissionGranted()) {
                        Glide.with(activity).load(jobImagesModels.get(vpImageSlider.getCurrentItem()).getUrl()).into(new SimpleTarget<Drawable>() {
                            @Override
                            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                              shareImage(resource);
                            }

                            @Override
                            public void onLoadFailed(@Nullable Drawable errorDrawable) {
                                super.onLoadFailed(errorDrawable);

                            }

                            @Override
                            public void onLoadStarted(@Nullable Drawable placeholder) {
                                super.onLoadStarted(placeholder);
                            }
                        });
                    } else {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    }
                }

            }

            @Override
            public void onExpand() {
//                showToast("onExpand");
            }

            @Override
            public void onCollapse() {
//                showToast("onCollapse");
            }
        });
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (activity.checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {

                return true;
            } else {


                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation

            return true;
        }
    }

    public void shareImageWhatsApp(Drawable drawable) {
        Bitmap adv = ((BitmapDrawable) drawable).getBitmap();
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/jpeg");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        adv.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File f = new File(Environment.getExternalStorageDirectory()
                + File.separator + "temporary_file.jpg");
        try {
            f.createNewFile();
            new FileOutputStream(f).write(bytes.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
        share.putExtra(Intent.EXTRA_STREAM,
                Uri.parse(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg"));
        if (isPackageInstalled("com.whatsapp", activity)) {
            share.setPackage("com.whatsapp");
            activity.startActivity(Intent.createChooser(share, "Share Image"));

        } else {

            Toast.makeText(activity, "Please Install Whatsapp", Toast.LENGTH_LONG).show();
        }

    }

    // Method to share any image.
    private void shareImage(Drawable drawable) {
        Bitmap adv = ((BitmapDrawable) drawable).getBitmap();
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/jpeg");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        adv.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg");
        try {
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
        share.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/temporary_file.jpg"));
        activity.startActivity(Intent.createChooser(share, "Share Image"));


    }


    private boolean isPackageInstalled(String packagename, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }


}