package com.whatsappdp.Utility;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import com.wang.avi.AVLoadingIndicatorView;
import com.whatsappdp.Modal.Upload;
import com.whatsappdp.R;

import java.util.ArrayList;

public class PostDetailsPagerAdapter extends android.support.v4.view.PagerAdapter {

    private Context context = null;
    private ArrayList<Upload> imgList = null;
    private RequestManager requestManager = null;
    int position=0;

    public PostDetailsPagerAdapter(Context context) {
        this.context = context;
        requestManager = Glide.with(context);
    }

    public PostDetailsPagerAdapter(Context context, ArrayList<Upload> imgList, int positions) {
        this.context = context;
        this.imgList = imgList;
        this.position = positions;
        requestManager = Glide.with(context);
    }

    @Override
    public int getCount() {
        return imgList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = infalInflater.inflate(R.layout.post_details_pager_adapter, container, false);
        final ViewHolderPage imgUserProfilePic = new ViewHolderPage();

        imgUserProfilePic.imgZoomable = itemView.findViewById(R.id.imgZoomable);
        imgUserProfilePic.avLoaderImage = itemView.findViewById(R.id.avLoaderImage);
        requestManager.load(imgList.get(position).getUrl()).into(new SimpleTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                imgUserProfilePic.imgZoomable.setImageDrawable(resource);
                imgUserProfilePic.avLoaderImage.hide();
            }

            @Override
            public void onLoadFailed(@Nullable Drawable errorDrawable) {
                super.onLoadFailed(errorDrawable);
                imgUserProfilePic.avLoaderImage.hide();

            }

            @Override
            public void onLoadStarted(@Nullable Drawable placeholder) {
                super.onLoadStarted(placeholder);
                imgUserProfilePic.avLoaderImage.show();


            }
        });
        container.addView(itemView);

        return itemView;
    }

    private class ViewHolderPage {
        private TouchImageView imgZoomable;
        private AVLoadingIndicatorView avLoaderImage;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}