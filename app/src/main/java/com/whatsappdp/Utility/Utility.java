package com.whatsappdp.Utility;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;



import org.json.JSONArray;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by dell on 12/7/2017.
 */

public class Utility {

    public static void redirectToActivity(Activity yourActivity, Class SecondActivity, Context context, boolean isfinish, Bundle b) {

        try {

            if (yourActivity != null) {
                Intent intent = new Intent(yourActivity, SecondActivity);

                if (b != null)
                    intent.putExtras(b);

                yourActivity.startActivity(intent);

                yourActivity.overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

                if (isfinish) {

                    yourActivity.finish();
                }
            } else

            {

                Intent intent = new Intent(context, SecondActivity);

                Activity activity = (Activity) context;

                if (b != null)
                    intent.putExtras(b);

                activity.startActivity(intent);

                activity.overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

                if (isfinish) {
                    activity.finish();
                }
            }


        } catch (Exception e) {
            Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
        }


    }
    public static String timeStamptoDate(long timeStamp) {

        try {
            DateFormat sdf = new SimpleDateFormat("dd MMM yyyy ");
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        } catch (Exception ex) {
            return "xx";
        }
    }
    private static final String TAG = "Utils";



    private static String loadJSONFromAsset(Context context, String jsonFileName) {
        String json = null;
        InputStream is = null;
        try {
            AssetManager manager = context.getAssets();
            Log.d(TAG, "path " + jsonFileName);
            is = manager.open(jsonFileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


    public static Point getDisplaySize(WindowManager windowManager) {
        try {
            if (Build.VERSION.SDK_INT > 16) {
                Display display = windowManager.getDefaultDisplay();
                DisplayMetrics displayMetrics = new DisplayMetrics();
                display.getMetrics(displayMetrics);
                return new Point(displayMetrics.widthPixels, displayMetrics.heightPixels);
            } else {
                return new Point(0, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new Point(0, 0);
        }
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static void fullScreenLayout(Activity activity)
    {
        activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
    public static int getPxFromDp(Context context, float dpUnits) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpUnits, context.getResources().getDisplayMetrics());
    }

    public static Bitmap decodeUri(Context context, Uri selectedImage) throws FileNotFoundException {
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(context.getContentResolver().openInputStream(selectedImage), null, o);
        final int REQUIRED_SIZE = 200;
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(context.getContentResolver().openInputStream(selectedImage), null, o2);


    }
    public static Typeface  setHeavyFont (TextView textView, Activity activity)
    {
        Typeface face = Typeface.createFromAsset(activity.getAssets(),
                "fonts/Avenir LT 85 Heavy.ttf");
        textView.setTypeface(face);

        return face;
    }
    public static Typeface  setRomanFont (TextView textView, Activity activity)
    {
        Typeface face = Typeface.createFromAsset(activity.getAssets(),
                "fonts/Avenir LT 55 Roman.ttf");
        textView.setTypeface(face);

        return face;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void showToast(Context context,String message){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
    public static String convertlatlongtoaddress(Double lat, Double lon, Context context, String type)

    {

        String addresss="";

        Geocoder geocoder = new Geocoder(context, Locale.getDefault());




        List<Address> addresses = new ArrayList<Address>();
        try {
            Log.d("latlong==>", lat.toString() + "  " + lon.toString());
            addresses = geocoder.getFromLocation(lat, lon, 1);

        } catch (IOException e) {
            e.printStackTrace();
        }

        if (addresses != null && addresses.size() > 0) {


            String addressIndex0 = (addresses.get(0).getAddressLine(0) != null) ? addresses
                    .get(0).getAddressLine(0) : null;
            String addressIndex1 = (addresses.get(0).getAddressLine(1) != null) ? addresses
                    .get(0).getAddressLine(1) : null;
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            if(city==null) {
                city = "";
            }

            if(type.equals("city"))
            {
                if(city.equals(""))
                {

                    addresss = country;
                }else {
                    addresss = city;
                }


            }else {
                addresss = city + ", "+ state  + ", "+ country  ;
            }




        }
        return addresss;

    }

    public static int getDisHeight(Activity act) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        act.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    public static int getDisWidth(Activity act) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        act.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }
    public static class TypefaceUtil {

        /**
         * Using reflection to override default typeface
         * NOTICE: DO NOT FORGET TO SET TYPEFACE FOR APP THEME AS DEFAULT TYPEFACE WHICH WILL BE OVERRIDDEN
         * @param context to work with assets
         * @param defaultFontNameToOverride for example "monospace"
         * @param customFontFileNameInAssets file name of the font from assets
         */
        public static void overrideFont(Context context, String defaultFontNameToOverride, String customFontFileNameInAssets) {
            try {
                final Typeface customFontTypeface = Typeface.createFromAsset(context.getAssets(), customFontFileNameInAssets);

                final Field defaultFontTypefaceField = Typeface.class.getDeclaredField(defaultFontNameToOverride);
                defaultFontTypefaceField.setAccessible(true);
                defaultFontTypefaceField.set(null, customFontTypeface);
            } catch (Exception e) {
               // Log.e("Can not set custom font " + customFontFileNameInAssets + " instead of " + defaultFontNameToOverride);
            }
        }
    }

}
