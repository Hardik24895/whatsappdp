package com.whatsappdp;

import android.app.Application;
import android.os.StrictMode;

import com.google.android.gms.ads.MobileAds;
import com.whatsappdp.Utility.Utility;
//import com.google.android.gms.ads.MobileAds;
import in.myinnos.customfontlibrary.TypefaceUtil;


public class AppBaseApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
    /*    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();


        StrictMode.setThreadPolicy(policy);*/
        // custom font for entire App
      TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Montserrat-Regular.ttf");
        // initialize the AdMob app
        MobileAds.initialize(this, getString(R.string.admob_app_id));
        }

}